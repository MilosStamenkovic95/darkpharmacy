﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Driver;

namespace DarkPharmacy.Entiteti
{
    public class MedicineStorage
    {
        public ObjectId Id { get; set; }
        public List<MongoDBRef> medicineBlocks { get; set; }

        public string storageName { get; set; }

        public MedicineStorage()
        {
            medicineBlocks = new List<MongoDBRef>();
        }
    }
}
