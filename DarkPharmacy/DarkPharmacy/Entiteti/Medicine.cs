﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Driver;

namespace DarkPharmacy.Entiteti
{
     public class Medicine
    {
        public ObjectId Id { get; set; }
        public string name { get; set; }
        public string scientificName { get; set; }
        public float price { get; set; }
        public float recipePrice { get; set; }
        public List<string> unwantedEffect { get; set; }

        public Medicine()
        {

        }
    }
}
