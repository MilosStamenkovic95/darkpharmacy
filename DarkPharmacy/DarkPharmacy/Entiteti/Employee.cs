﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Driver;

namespace DarkPharmacy.Entiteti
{
   public class Employee
    {
        public ObjectId Id { get; set; }
        public string name { get; set; }
        public DateTime birthDate { get; set; }
        public bool intern { get; set; }
        public float payment { get; set; }
    }
}
