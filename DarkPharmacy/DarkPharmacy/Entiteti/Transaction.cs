﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Driver;

namespace DarkPharmacy.Entiteti
{
    public class Transaction
    {
        public ObjectId Id;
        public MongoDBRef pharma { get; set; }
        public MongoDBRef employee { get; set; }
        public MongoDBRef recipe { get; set; }
        public float totalPrice { get; set; }
        public List<MongoDBRef> medicines { get; set; }
        public Transaction()
        {
            medicines = new List<MongoDBRef>();
        }
    }
}
