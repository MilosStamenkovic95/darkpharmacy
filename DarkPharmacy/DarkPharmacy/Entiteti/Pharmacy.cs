﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Driver;

namespace DarkPharmacy.Entiteti
{
    public class Pharmacy
    {
        public ObjectId Id;
        public string address { get; set; }
        public List<MongoDBRef> medicineStorage { get; set; }
        public List<MongoDBRef> employees { get; set; }
        public Pharmacy()
        {
            medicineStorage = new List<MongoDBRef>();
            employees = new List<MongoDBRef>();
        }
    }
}
