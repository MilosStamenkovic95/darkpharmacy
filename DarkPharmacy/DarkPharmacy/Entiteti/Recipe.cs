﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Driver;

namespace DarkPharmacy.Entiteti
{
    public class Recipe
    {
        public ObjectId Id { get; set; }
        public bool seal { get; set; }
        public List<String> allMedicines { get; set; }
        public string idRecipe; 
        public Recipe()
        {
            allMedicines = new List<String>();
        }
    }

    public class RecipeCountViewModel
    {
        public string Name;
        public int Amount;

        public RecipeCountViewModel()
        {

        }
    }
}
