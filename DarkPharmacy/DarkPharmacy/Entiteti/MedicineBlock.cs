﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MongoDB.Bson;
using MongoDB.Driver;

namespace DarkPharmacy.Entiteti
{
    public class MedicineBlock
    {
        public ObjectId id { get; set; }
        public int count { get; set; }
        public DateTime expirationDate { get; set; }
        public bool state { get; set; }
        public int treshholdState { get; set; } //in months (1-12) ? 13 maybe :P
    }
}
