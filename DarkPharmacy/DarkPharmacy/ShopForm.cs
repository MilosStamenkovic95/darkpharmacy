﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using DarkPharmacy.Entiteti;

namespace DarkPharmacy
{
    public partial class ShopForm : Form
    {
        private Employee _currentEmplyee;

        public bool verifikacija;


        public ShopForm()
        {
            InitializeComponent();
        }

        public string connectionString;
        public static int redisBeforePurchase = 0;
        public MongoServer server;
        public MongoDatabase db;

        public void prepareConnection()
        {
            connectionString = "mongodb://localhost/?safe=true";
            server = MongoServer.Create(connectionString);
            db = server.GetDatabase("apoteka");
        }

        private void VerificationBtn_Click_1(object sender, EventArgs e)
        {
            prepareConnection();

            _currentEmplyee = db.GetCollection<Employee>("radnici").AsQueryable<Employee>().Where(m => m.name == EmployeeNameTxt.Text).FirstOrDefault();
            if (_currentEmplyee == null)
            {
                MessageBox.Show("No results");
            }
            else
            {
                verifikacija = true;
                MessageBox.Show(_currentEmplyee.name.ToString() + " je uspesno prijavljen/a");
            }
        }

        private void CalculateSumToPaybtn_Click(object sender, EventArgs e)
        {

            prepareConnection();

            List<RecipeCountViewModel> medicsToShip = new List<RecipeCountViewModel>();
            //parse listbox
            foreach (var item in listBox1.Items)
            {
                string itemString = item.ToString();
                string[] splitedItem = itemString.Split(' ');
                if (splitedItem[0] == "NEMA")
                    continue;
                medicsToShip.Add(ParsePaycheckItem(splitedItem));
            }

            //do the maths
            foreach (var medicToShip in medicsToShip)
            {
                //get storage
                MedicineStorage medicineStorage = db.GetCollection<MedicineStorage>("skladiste").AsQueryable<MedicineStorage>().
                        Where(m => m.storageName == medicToShip.Name).FirstOrDefault();

                //get blocks for this medic
                List<MedicineBlock> blocks = new List<MedicineBlock>(medicineStorage.medicineBlocks.Count);
                foreach (var item in medicineStorage.medicineBlocks)
                {

                    blocks.Add(db.GetCollection<MedicineBlock>("blok").AsQueryable<MedicineBlock>().
                        Where(m => m.id == item.Id.AsObjectId).FirstOrDefault());
                }

                if (medicsToShip.First() == medicToShip)
                {
                    foreach (var item in medicineStorage.medicineBlocks)
                    {
                        RedisMedicine redMed = new RedisMedicine();
                        redisBeforePurchase = redMed.CheckReducingAmmounts(item.Id.AsObjectId, redisBeforePurchase);
                    }
                }


                #region TODO TASKS
                //**//****************************************************************
                //**//TODO: 1. amount < block.count => easy
                //**//2. amont == block.count => med, delete block after
                //**//3. amont > block.count => hard; 1st block clear->delete; amountRest = amount - block.count; 2nd block.count - amountRest;
                //**//3.is recursive!!!!!
                //**//*****************************************************************
                #endregion

                //1.Amount of one type  medic in all blocks in storage-Completed
                #region MedicinesAmountCalculating
                int totalAmount = 0;
                foreach (var amountInBlocks in blocks)
                {
                    totalAmount += amountInBlocks.count;
                }
                MessageBox.Show("Kolicina: " + totalAmount.ToString() + " " + medicToShip.Name);
                #endregion

                //2. && 3. Sell medicines
                #region Sell medicines and Update
                foreach (var amountInOneBlock in blocks)
                {
                    if (medicToShip.Amount == amountInOneBlock.count)
                    {
                        #region explanation condition
                        //ako je broj kupljenih lekova jednak jednom bloku trebamo da izbrisemo ceo blok
                        //za sada stavljamo da je count u bloku 0 ako kupimo sve lekove jednog bloka
                        //(dok ne nadjemo resenje za brisanje  bloka ciji je count 0)
                        #endregion

                        amountInOneBlock.count = 0;
                        UpdateCountInBlock(amountInOneBlock.count, amountInOneBlock.id);
                        //DeleteBlock(amountInOneBlock.id); //ovo uklanja blok ali treba i referencu da uklonimo iz storage dok to ne uklonimo ovo je problem
                        MessageBox.Show("Kupljeni su svi lekovi iz jednog boxa lekova " + medicToShip.Name);
                        RedisMedicine redMed = new RedisMedicine();
                        redMed.SetMedBlockCountById(amountInOneBlock.id, amountInOneBlock.count);
                        break;
                    }
                    else if (medicToShip.Amount < amountInOneBlock.count)
                    {
                        #region explanation condition
                        //ako kolicina kupljenih lekova manja nego broj lekova jednog bloka 
                        //smanjimo broj preostale zalihe uokviru jednog bloka
                        #endregion

                        amountInOneBlock.count = amountInOneBlock.count - medicToShip.Amount;
                        UpdateCountInBlock(amountInOneBlock.count, amountInOneBlock.id);
                        MessageBox.Show("Presotale su " + amountInOneBlock.count + " kutije lekova " + medicToShip.Name);
                        RedisMedicine redMed = new RedisMedicine();
                        redMed.SetMedBlockCountById(amountInOneBlock.id, amountInOneBlock.count);
                        break;
                    }
                    else if (medicToShip.Amount > amountInOneBlock.count && medicToShip.Amount > 0 && amountInOneBlock.count > 0)
                    {
                        #region explanation condition
                        //ako kolicina kupljenih lekova veca nego broj lekova jednog bloka 
                        //smanjimo broj lekova koji kupujemo za velicinu tog bloka
                        //izbrisemo taj blok  i prelazimo na sledeci blok
                        //dok broj lekova koji kupujemo nije nula
                        //amountInOneBlock != null jer cemo da brisemo blokove
                        #endregion

                        medicToShip.Amount = medicToShip.Amount - amountInOneBlock.count;
                        amountInOneBlock.count = 0;
                        UpdateCountInBlock(amountInOneBlock.count, amountInOneBlock.id);
                        //DeleteBlock(amountInOneBlock.id);
                        MessageBox.Show("Idemo po novi blok u magacin " + medicToShip.Name);
                        RedisMedicine redMed = new RedisMedicine();
                        redMed.SetMedBlockCountById(amountInOneBlock.id, amountInOneBlock.count);
                        continue;
                    }

                    #region explanation for last condition
                    //ako nam je count u okviru bloka 0 nemamo vise lekova u tom bloku i prelazimo na sledeci blok
                    #endregion
                    else continue;
                }
                #endregion

                #region check  totalAmount after buying medicines

                totalAmount = 0;
                foreach (var amountInBlocks in blocks)
                {
                    totalAmount += amountInBlocks.count;
                }
                if (totalAmount == 0)
                    MessageBox.Show("Nestalo nam je " + medicToShip.Name + " lekova,nema ni u magacinu");
                else
                    MessageBox.Show("Preostalo je ukupno " + totalAmount + " kutija " + medicToShip.Name + " u celom skladistu");
                #endregion

                int redisBlockCount = 0;
                foreach (var item in medicineStorage.medicineBlocks)
                {
                    RedisMedicine redMed = new RedisMedicine();
                    redisBlockCount = redMed.CheckReducingAmmounts(item.Id.AsObjectId, redisBlockCount);
                }
                if (redisBlockCount <= 5)
                {
                    MessageBox.Show("Upozorenje: lek " + medicToShip.Name + " ima 5 ili manje kutija na stanju");
                }
                if (redisBeforePurchase - redisBlockCount > 50)
                {
                    MessageBox.Show("Upozorenje: lek " + medicToShip.Name + " se prodaje u velikim kolicinama, povecati nabavku");
                }
            }
            listBox1.Items.Clear();
        }

        #region ChangeBlockOperation
        //Function to Update count in block when we buy medicines
        private void UpdateCountInBlock(int count, ObjectId id)
        {
            var collection = db.GetCollection<MedicineBlock>("blok");
            var query = Query.EQ("_id", id);
            var Update = MongoDB.Driver.Builders.Update.Set("count", BsonValue.Create(count));
            collection.Update(query, Update);
        }

        //Function to Delete empty count in block when we buy medicines but we need to delete ref in Storage also..
        private void DeleteBlock(ObjectId id)
        {
            var collection = db.GetCollection<MedicineBlock>("blok");
            var query = Query.EQ("_id", id);
            collection.Remove(query);
        }
        #endregion


        private RecipeCountViewModel ParsePaycheckItem(string[] item)
        {
            RecipeCountViewModel medicParsed = new RecipeCountViewModel()
            {
                Name = item[0],
                Amount = int.Parse(item[2])
            };
            return medicParsed;
        }

        private void showMedicinesBnt_Click(object sender, EventArgs e)
        {
            if (verifikacija)
            {
                prepareConnection();
                var collectionRecepie = db.GetCollection<Recipe>("recept");

                Recipe recept = new Recipe
                {
                    idRecipe = RecipeIdTxt.Text,
                    seal = checkBoxSeal.Checked,
                };

                foreach (var item in MedicinesOnRecipe.Lines)
                {
                    recept.allMedicines.Add(item);
                }

                CreatePrecheck(ParseRecipie(), recept.seal);

                collectionRecepie.Insert(recept);
                MessageBox.Show("Uspesno dodat recept sa lekovima: \n" + MedicinesOnRecipe.Text);
            
            }
        else
        {
                MessageBox.Show("Morate se prvo verifikovati da bi ste mogli da unosite recept i vrsite transakciju lekova");
        }
        }
        private void CreatePrecheck(List<RecipeCountViewModel> recipe, bool seal)
        {
            List<float> pricesOfMedicies = new List<float>();
           
            //TODO:clean up this
            if (recipe == null)
            {
                MessageBox.Show("No results");
            }
            else
            {
                foreach (RecipeCountViewModel lekic in recipe)
                {
                    var lekoviSaSpiska = db.GetCollection<Medicine>("lek").AsQueryable<Medicine>().
                        Where(m => m.name == lekic.Name).FirstOrDefault();

                    if (lekoviSaSpiska != null)
                    {
                        if (seal == false)
                        {
                            listBox1.Items.Add(lekic.Name + " kolicine: " + lekic.Amount + " cena: " + lekoviSaSpiska.price * lekic.Amount);
                            pricesOfMedicies.Add(lekoviSaSpiska.price * lekic.Amount);
                        }
                        else
                        { 
                            listBox1.Items.Add(lekic.Name + " kolicine: " + lekic.Amount + " cena: " + lekoviSaSpiska.recipePrice * lekic.Amount);
                            pricesOfMedicies.Add(lekoviSaSpiska.recipePrice * lekic.Amount);
                        }
                    }
                    else
                    {
                        listBox1.Items.Add("NEMA " + lekic.Name);
                        pricesOfMedicies.Add(0);
                    }
                }
                float sum = pricesOfMedicies.Aggregate(func: (result, item) => result + item);
                MessageBox.Show("Suma iznosi: " + sum.ToString());
            }
        }

        private List<RecipeCountViewModel> ParseRecipie()
        {
            List<RecipeCountViewModel> medicines = new List<RecipeCountViewModel>();
            foreach (var item in MedicinesOnRecipe.Lines)
            {
                RecipeCountViewModel recipeVM = new RecipeCountViewModel();
                string[] parsedItem = item.Split(' ');
                recipeVM.Name = parsedItem[0];
                if (parsedItem.Length == 1)
                {
                    recipeVM.Amount = 1;
                }
                else if (!int.TryParse(parsedItem[1], out recipeVM.Amount))
                {
                    recipeVM.Amount = 1;
                }
                medicines.Add(recipeVM);
            }
            return medicines;
        }
    }
}
