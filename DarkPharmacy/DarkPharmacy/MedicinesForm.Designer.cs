﻿namespace DarkPharmacy
{
    partial class MedicinesForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.DeleteMedicines = new System.Windows.Forms.Button();
            this.comboBoxMedic = new System.Windows.Forms.ComboBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.IsThereUnwantedEffectChckBox = new System.Windows.Forms.CheckBox();
            this.ChckListUnwantedEffect = new System.Windows.Forms.CheckedListBox();
            this.unwantedEffectTxt = new System.Windows.Forms.TextBox();
            this.recipePricetxt = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.AddMedicinesBtn = new System.Windows.Forms.Button();
            this.priceTxt = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.scientificNameTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.NameTxt = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.MedicinesCollectionBnt = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.NewPriceTxt = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.UpdateMedicineBtn = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.searchMedicineByNamebtn = new System.Windows.Forms.Button();
            this.NameSearchMedictxt = new System.Windows.Forms.TextBox();
            this.MatchByNameAndPrice = new System.Windows.Forms.Button();
            this.CountMedicinesBnt = new System.Windows.Forms.Button();
            this.SortByNameBnt = new System.Windows.Forms.Button();
            this.SortByPriceBnt = new System.Windows.Forms.Button();
            this.ReturnFirstBnt = new System.Windows.Forms.Button();
            this.ReturnOtherBnt = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.ExpCheckBtn = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.DeleteMedicines);
            this.groupBox2.Controls.Add(this.comboBoxMedic);
            this.groupBox2.Location = new System.Drawing.Point(263, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(173, 122);
            this.groupBox2.TabIndex = 16;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Delete Medicine";
            // 
            // DeleteMedicines
            // 
            this.DeleteMedicines.Location = new System.Drawing.Point(16, 57);
            this.DeleteMedicines.Name = "DeleteMedicines";
            this.DeleteMedicines.Size = new System.Drawing.Size(121, 40);
            this.DeleteMedicines.TabIndex = 14;
            this.DeleteMedicines.Text = "Delete Medicines";
            this.DeleteMedicines.UseVisualStyleBackColor = true;
            this.DeleteMedicines.Click += new System.EventHandler(this.DeleteMedicines_Click);
            // 
            // comboBoxMedic
            // 
            this.comboBoxMedic.FormattingEnabled = true;
            this.comboBoxMedic.Location = new System.Drawing.Point(16, 30);
            this.comboBoxMedic.Name = "comboBoxMedic";
            this.comboBoxMedic.Size = new System.Drawing.Size(121, 21);
            this.comboBoxMedic.TabIndex = 0;
            this.comboBoxMedic.Enter += new System.EventHandler(this.comboBoxMedic_Enter);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.groupBox5);
            this.groupBox1.Controls.Add(this.recipePricetxt);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.AddMedicinesBtn);
            this.groupBox1.Controls.Add(this.priceTxt);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.scientificNameTxt);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.NameTxt);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(236, 374);
            this.groupBox1.TabIndex = 15;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add new Medicine";
            // 
            // IsThereUnwantedEffectChckBox
            // 
            this.IsThereUnwantedEffectChckBox.AutoSize = true;
            this.IsThereUnwantedEffectChckBox.Location = new System.Drawing.Point(62, 18);
            this.IsThereUnwantedEffectChckBox.Name = "IsThereUnwantedEffectChckBox";
            this.IsThereUnwantedEffectChckBox.Size = new System.Drawing.Size(139, 17);
            this.IsThereUnwantedEffectChckBox.TabIndex = 28;
            this.IsThereUnwantedEffectChckBox.Text = "IsThereUnwantedEffect";
            this.IsThereUnwantedEffectChckBox.UseVisualStyleBackColor = true;
            // 
            // ChckListUnwantedEffect
            // 
            this.ChckListUnwantedEffect.FormattingEnabled = true;
            this.ChckListUnwantedEffect.Items.AddRange(new object[] {
            "Mucnina",
            "Povisena temeperatura",
            "Malaksalost",
            "Dijareja"});
            this.ChckListUnwantedEffect.Location = new System.Drawing.Point(63, 70);
            this.ChckListUnwantedEffect.Name = "ChckListUnwantedEffect";
            this.ChckListUnwantedEffect.Size = new System.Drawing.Size(143, 64);
            this.ChckListUnwantedEffect.TabIndex = 28;
            // 
            // unwantedEffectTxt
            // 
            this.unwantedEffectTxt.Location = new System.Drawing.Point(63, 41);
            this.unwantedEffectTxt.Multiline = true;
            this.unwantedEffectTxt.Name = "unwantedEffectTxt";
            this.unwantedEffectTxt.Size = new System.Drawing.Size(138, 23);
            this.unwantedEffectTxt.TabIndex = 28;
            // 
            // recipePricetxt
            // 
            this.recipePricetxt.Location = new System.Drawing.Point(92, 140);
            this.recipePricetxt.Name = "recipePricetxt";
            this.recipePricetxt.Size = new System.Drawing.Size(100, 20);
            this.recipePricetxt.TabIndex = 14;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(6, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 13);
            this.label5.TabIndex = 13;
            this.label5.Text = "Recipe Price";
            // 
            // AddMedicinesBtn
            // 
            this.AddMedicinesBtn.Location = new System.Drawing.Point(121, 334);
            this.AddMedicinesBtn.Name = "AddMedicinesBtn";
            this.AddMedicinesBtn.Size = new System.Drawing.Size(100, 34);
            this.AddMedicinesBtn.TabIndex = 12;
            this.AddMedicinesBtn.Text = "Add Medicine";
            this.AddMedicinesBtn.UseVisualStyleBackColor = true;
            this.AddMedicinesBtn.Click += new System.EventHandler(this.AddMedicinesBtn_Click);
            // 
            // priceTxt
            // 
            this.priceTxt.Location = new System.Drawing.Point(92, 102);
            this.priceTxt.Name = "priceTxt";
            this.priceTxt.Size = new System.Drawing.Size(100, 20);
            this.priceTxt.TabIndex = 11;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "Price";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(0, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "Scientific Name";
            // 
            // scientificNameTxt
            // 
            this.scientificNameTxt.Location = new System.Drawing.Point(92, 57);
            this.scientificNameTxt.Name = "scientificNameTxt";
            this.scientificNameTxt.Size = new System.Drawing.Size(100, 20);
            this.scientificNameTxt.TabIndex = 7;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Name";
            // 
            // NameTxt
            // 
            this.NameTxt.Location = new System.Drawing.Point(92, 20);
            this.NameTxt.Name = "NameTxt";
            this.NameTxt.Size = new System.Drawing.Size(100, 20);
            this.NameTxt.TabIndex = 5;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(44, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 14;
            // 
            // MedicinesCollectionBnt
            // 
            this.MedicinesCollectionBnt.Location = new System.Drawing.Point(6, 45);
            this.MedicinesCollectionBnt.Name = "MedicinesCollectionBnt";
            this.MedicinesCollectionBnt.Size = new System.Drawing.Size(78, 34);
            this.MedicinesCollectionBnt.TabIndex = 17;
            this.MedicinesCollectionBnt.Text = "Show Medicines";
            this.MedicinesCollectionBnt.UseVisualStyleBackColor = true;
            this.MedicinesCollectionBnt.Click += new System.EventHandler(this.MedicinesCollectionBnt_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.NewPriceTxt);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.UpdateMedicineBtn);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.searchMedicineByNamebtn);
            this.groupBox3.Controls.Add(this.NameSearchMedictxt);
            this.groupBox3.Controls.Add(this.MedicinesCollectionBnt);
            this.groupBox3.Location = new System.Drawing.Point(254, 152);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(378, 96);
            this.groupBox3.TabIndex = 15;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Show";
            // 
            // NewPriceTxt
            // 
            this.NewPriceTxt.Location = new System.Drawing.Point(302, 19);
            this.NewPriceTxt.Name = "NewPriceTxt";
            this.NewPriceTxt.Size = new System.Drawing.Size(70, 20);
            this.NewPriceTxt.TabIndex = 23;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(241, 23);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(55, 13);
            this.label7.TabIndex = 22;
            this.label7.Text = "New price";
            // 
            // UpdateMedicineBtn
            // 
            this.UpdateMedicineBtn.Location = new System.Drawing.Point(302, 47);
            this.UpdateMedicineBtn.Name = "UpdateMedicineBtn";
            this.UpdateMedicineBtn.Size = new System.Drawing.Size(70, 33);
            this.UpdateMedicineBtn.TabIndex = 21;
            this.UpdateMedicineBtn.Text = "Update";
            this.UpdateMedicineBtn.UseVisualStyleBackColor = true;
            this.UpdateMedicineBtn.Click += new System.EventHandler(this.UpdateMedicineBtn_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(6, 22);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 13);
            this.label6.TabIndex = 20;
            this.label6.Text = "Enter name of medicine";
            // 
            // searchMedicineByNamebtn
            // 
            this.searchMedicineByNamebtn.Location = new System.Drawing.Point(135, 45);
            this.searchMedicineByNamebtn.Name = "searchMedicineByNamebtn";
            this.searchMedicineByNamebtn.Size = new System.Drawing.Size(100, 34);
            this.searchMedicineByNamebtn.TabIndex = 19;
            this.searchMedicineByNamebtn.Text = "Search";
            this.searchMedicineByNamebtn.UseVisualStyleBackColor = true;
            this.searchMedicineByNamebtn.Click += new System.EventHandler(this.searchMedicineByNamebtn_Click);
            // 
            // NameSearchMedictxt
            // 
            this.NameSearchMedictxt.Location = new System.Drawing.Point(135, 19);
            this.NameSearchMedictxt.Name = "NameSearchMedictxt";
            this.NameSearchMedictxt.Size = new System.Drawing.Size(100, 20);
            this.NameSearchMedictxt.TabIndex = 18;
            // 
            // MatchByNameAndPrice
            // 
            this.MatchByNameAndPrice.Location = new System.Drawing.Point(188, 100);
            this.MatchByNameAndPrice.Name = "MatchByNameAndPrice";
            this.MatchByNameAndPrice.Size = new System.Drawing.Size(155, 31);
            this.MatchByNameAndPrice.TabIndex = 24;
            this.MatchByNameAndPrice.Text = "Match by Name and price";
            this.MatchByNameAndPrice.UseVisualStyleBackColor = true;
            this.MatchByNameAndPrice.Click += new System.EventHandler(this.MatchByNameAndPrice_Click);
            // 
            // CountMedicinesBnt
            // 
            this.CountMedicinesBnt.Location = new System.Drawing.Point(13, 22);
            this.CountMedicinesBnt.Name = "CountMedicinesBnt";
            this.CountMedicinesBnt.Size = new System.Drawing.Size(155, 31);
            this.CountMedicinesBnt.TabIndex = 17;
            this.CountMedicinesBnt.Text = "Count Medicines";
            this.CountMedicinesBnt.UseVisualStyleBackColor = true;
            this.CountMedicinesBnt.Click += new System.EventHandler(this.CountMedicinesBnt_Click_1);
            // 
            // SortByNameBnt
            // 
            this.SortByNameBnt.Location = new System.Drawing.Point(13, 59);
            this.SortByNameBnt.Name = "SortByNameBnt";
            this.SortByNameBnt.Size = new System.Drawing.Size(155, 34);
            this.SortByNameBnt.TabIndex = 23;
            this.SortByNameBnt.Text = "Sort medicines by name";
            this.SortByNameBnt.UseVisualStyleBackColor = true;
            this.SortByNameBnt.Click += new System.EventHandler(this.SortByNameBnt_Click);
            // 
            // SortByPriceBnt
            // 
            this.SortByPriceBnt.Location = new System.Drawing.Point(13, 100);
            this.SortByPriceBnt.Name = "SortByPriceBnt";
            this.SortByPriceBnt.Size = new System.Drawing.Size(155, 31);
            this.SortByPriceBnt.TabIndex = 24;
            this.SortByPriceBnt.Text = "Sort medicines by price";
            this.SortByPriceBnt.UseVisualStyleBackColor = true;
            this.SortByPriceBnt.Click += new System.EventHandler(this.SortByPriceBnt_Click);
            // 
            // ReturnFirstBnt
            // 
            this.ReturnFirstBnt.Location = new System.Drawing.Point(188, 61);
            this.ReturnFirstBnt.Name = "ReturnFirstBnt";
            this.ReturnFirstBnt.Size = new System.Drawing.Size(155, 31);
            this.ReturnFirstBnt.TabIndex = 25;
            this.ReturnFirstBnt.Text = "Return fisrt medicines";
            this.ReturnFirstBnt.UseVisualStyleBackColor = true;
            this.ReturnFirstBnt.Click += new System.EventHandler(this.ReturnFirstBnt_Click);
            // 
            // ReturnOtherBnt
            // 
            this.ReturnOtherBnt.Location = new System.Drawing.Point(188, 22);
            this.ReturnOtherBnt.Name = "ReturnOtherBnt";
            this.ReturnOtherBnt.Size = new System.Drawing.Size(155, 31);
            this.ReturnOtherBnt.TabIndex = 26;
            this.ReturnOtherBnt.Text = "Return other two";
            this.ReturnOtherBnt.UseVisualStyleBackColor = true;
            this.ReturnOtherBnt.Click += new System.EventHandler(this.ReturnOtherBnt_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.ExpCheckBtn);
            this.groupBox4.Location = new System.Drawing.Point(638, 159);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(192, 85);
            this.groupBox4.TabIndex = 27;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Redis tests";
            // 
            // ExpCheckBtn
            // 
            this.ExpCheckBtn.Location = new System.Drawing.Point(55, 34);
            this.ExpCheckBtn.Name = "ExpCheckBtn";
            this.ExpCheckBtn.Size = new System.Drawing.Size(117, 36);
            this.ExpCheckBtn.TabIndex = 0;
            this.ExpCheckBtn.Text = "Check expiration";
            this.ExpCheckBtn.UseVisualStyleBackColor = true;
            this.ExpCheckBtn.Click += new System.EventHandler(this.ExpCheckBtn_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.IsThereUnwantedEffectChckBox);
            this.groupBox5.Controls.Add(this.ChckListUnwantedEffect);
            this.groupBox5.Controls.Add(this.unwantedEffectTxt);
            this.groupBox5.Location = new System.Drawing.Point(6, 174);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(221, 154);
            this.groupBox5.TabIndex = 29;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Unwanted Effect";
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.MatchByNameAndPrice);
            this.groupBox6.Controls.Add(this.ReturnOtherBnt);
            this.groupBox6.Controls.Add(this.ReturnFirstBnt);
            this.groupBox6.Controls.Add(this.SortByPriceBnt);
            this.groupBox6.Controls.Add(this.SortByNameBnt);
            this.groupBox6.Controls.Add(this.CountMedicinesBnt);
            this.groupBox6.Location = new System.Drawing.Point(458, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(351, 140);
            this.groupBox6.TabIndex = 28;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Other Tests";
            // 
            // MedicinesForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(831, 395);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Name = "MedicinesForm";
            this.Text = "Medicines";
            this.groupBox2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.groupBox6.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button DeleteMedicines;
        private System.Windows.Forms.ComboBox comboBoxMedic;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button AddMedicinesBtn;
        private System.Windows.Forms.TextBox priceTxt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox scientificNameTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox NameTxt;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox recipePricetxt;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button MedicinesCollectionBnt;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Button searchMedicineByNamebtn;
        private System.Windows.Forms.TextBox NameSearchMedictxt;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button UpdateMedicineBtn;
        private System.Windows.Forms.TextBox NewPriceTxt;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Button CountMedicinesBnt;
        private System.Windows.Forms.Button SortByNameBnt;
        private System.Windows.Forms.Button SortByPriceBnt;
        private System.Windows.Forms.Button MatchByNameAndPrice;
        private System.Windows.Forms.Button ReturnFirstBnt;
        private System.Windows.Forms.Button ReturnOtherBnt;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button ExpCheckBtn;
        private System.Windows.Forms.TextBox unwantedEffectTxt;
        private System.Windows.Forms.CheckedListBox ChckListUnwantedEffect;
        private System.Windows.Forms.CheckBox IsThereUnwantedEffectChckBox;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox6;
    }
}