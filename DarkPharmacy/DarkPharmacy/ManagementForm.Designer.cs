﻿namespace DarkPharmacy
{
    partial class ManagementForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.EmployeeCollectionBnt = new System.Windows.Forms.Button();
            this.NameTxt = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.PaymentTxt = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.EmployeeBirthDate = new System.Windows.Forms.DateTimePicker();
            this.intern = new System.Windows.Forms.CheckBox();
            this.AddEmployeeBtn1 = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.comboBoxEmployee = new System.Windows.Forms.ComboBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // EmployeeCollectionBnt
            // 
            this.EmployeeCollectionBnt.Location = new System.Drawing.Point(6, 22);
            this.EmployeeCollectionBnt.Name = "EmployeeCollectionBnt";
            this.EmployeeCollectionBnt.Size = new System.Drawing.Size(113, 40);
            this.EmployeeCollectionBnt.TabIndex = 3;
            this.EmployeeCollectionBnt.Text = "Show Employees";
            this.EmployeeCollectionBnt.UseVisualStyleBackColor = true;
            this.EmployeeCollectionBnt.Click += new System.EventHandler(this.EmployeeCollectionBnt_Click_1);
            // 
            // NameTxt
            // 
            this.NameTxt.Location = new System.Drawing.Point(85, 19);
            this.NameTxt.Name = "NameTxt";
            this.NameTxt.Size = new System.Drawing.Size(100, 20);
            this.NameTxt.TabIndex = 5;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(27, 20);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(193, 186);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(0, 13);
            this.label2.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(27, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(50, 13);
            this.label3.TabIndex = 9;
            this.label3.Text = "birthDate";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(24, 109);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(47, 13);
            this.label4.TabIndex = 10;
            this.label4.Text = "payment";
            // 
            // PaymentTxt
            // 
            this.PaymentTxt.Location = new System.Drawing.Point(85, 102);
            this.PaymentTxt.Name = "PaymentTxt";
            this.PaymentTxt.Size = new System.Drawing.Size(100, 20);
            this.PaymentTxt.TabIndex = 11;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.EmployeeBirthDate);
            this.groupBox1.Controls.Add(this.intern);
            this.groupBox1.Controls.Add(this.AddEmployeeBtn1);
            this.groupBox1.Controls.Add(this.PaymentTxt);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.NameTxt);
            this.groupBox1.Location = new System.Drawing.Point(161, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(305, 236);
            this.groupBox1.TabIndex = 12;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add new Employee";
            // 
            // EmployeeBirthDate
            // 
            this.EmployeeBirthDate.Location = new System.Drawing.Point(83, 53);
            this.EmployeeBirthDate.Name = "EmployeeBirthDate";
            this.EmployeeBirthDate.Size = new System.Drawing.Size(196, 20);
            this.EmployeeBirthDate.TabIndex = 14;
            // 
            // intern
            // 
            this.intern.AutoSize = true;
            this.intern.Location = new System.Drawing.Point(85, 141);
            this.intern.Name = "intern";
            this.intern.Size = new System.Drawing.Size(52, 17);
            this.intern.TabIndex = 13;
            this.intern.Text = "intern";
            this.intern.UseVisualStyleBackColor = true;
            
            // 
            // AddEmployeeBtn1
            // 
            this.AddEmployeeBtn1.Location = new System.Drawing.Point(83, 174);
            this.AddEmployeeBtn1.Name = "AddEmployeeBtn1";
            this.AddEmployeeBtn1.Size = new System.Drawing.Size(100, 34);
            this.AddEmployeeBtn1.TabIndex = 12;
            this.AddEmployeeBtn1.Text = "Add Employee";
            this.AddEmployeeBtn1.UseVisualStyleBackColor = true;
            this.AddEmployeeBtn1.Click += new System.EventHandler(this.AddEmployeeBtn1_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.button1);
            this.groupBox2.Controls.Add(this.comboBoxEmployee);
            this.groupBox2.Location = new System.Drawing.Point(489, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(207, 234);
            this.groupBox2.TabIndex = 13;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Delete Employee";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(16, 57);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(131, 40);
            this.button1.TabIndex = 14;
            this.button1.Text = "Delete Employee";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.DeleteEmpoyeeBtn1_Click);
            // 
            // comboBoxEmployee
            // 
            this.comboBoxEmployee.FormattingEnabled = true;
            this.comboBoxEmployee.Location = new System.Drawing.Point(16, 22);
            this.comboBoxEmployee.Name = "comboBoxEmployee";
            this.comboBoxEmployee.Size = new System.Drawing.Size(121, 21);
            this.comboBoxEmployee.TabIndex = 0;
            this.comboBoxEmployee.Enter += new System.EventHandler(this.comboBoxEmployee_Enter);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.EmployeeCollectionBnt);
            this.groupBox3.Location = new System.Drawing.Point(12, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(143, 234);
            this.groupBox3.TabIndex = 14;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Show Employees";
            // 
            // ManagementForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(708, 258);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Name = "ManagementForm";
            this.Text = "Management";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button EmployeeCollectionBnt;
        private System.Windows.Forms.TextBox NameTxt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox PaymentTxt;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button AddEmployeeBtn1;
        private System.Windows.Forms.CheckBox intern;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.ComboBox comboBoxEmployee;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DateTimePicker EmployeeBirthDate;
        private System.Windows.Forms.GroupBox groupBox3;
    }
}