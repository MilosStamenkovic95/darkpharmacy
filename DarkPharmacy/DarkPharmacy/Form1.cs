﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DarkPharmacy
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        
        private void ManagementBtn_Click(object sender, EventArgs e)
        {
            ManagementForm form = new ManagementForm();
            form.Show();
            //this.Hide();
        }

        private void StorageBtn_Click(object sender, EventArgs e)
        {
            StorageForm form = new StorageForm();
            form.Show();
            //this.Hide();
        }

        private void ShopBnt_Click(object sender, EventArgs e)
        {
            ShopForm form = new ShopForm();
            form.Show();
            //this.Hide();
        }

        private void MedicinesBtn_Click(object sender, EventArgs e)
        {
            MedicinesForm form = new MedicinesForm();
            form.Show();
            //this.Hide();
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
