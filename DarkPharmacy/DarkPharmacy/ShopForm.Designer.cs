﻿namespace DarkPharmacy
{
    partial class ShopForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.EmployeeNameTxt = new System.Windows.Forms.TextBox();
            this.VerificationBtn = new System.Windows.Forms.Button();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.CalculateSumToPaybtn = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label4 = new System.Windows.Forms.Label();
            this.RecipeIdTxt = new System.Windows.Forms.TextBox();
            this.showMedicinesBnt = new System.Windows.Forms.Button();
            this.MedicinesOnRecipe = new System.Windows.Forms.TextBox();
            this.checkBoxSeal = new System.Windows.Forms.CheckBox();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(19, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(53, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Employee";
            // 
            // EmployeeNameTxt
            // 
            this.EmployeeNameTxt.Location = new System.Drawing.Point(22, 30);
            this.EmployeeNameTxt.Name = "EmployeeNameTxt";
            this.EmployeeNameTxt.Size = new System.Drawing.Size(116, 20);
            this.EmployeeNameTxt.TabIndex = 1;
            // 
            // VerificationBtn
            // 
            this.VerificationBtn.Location = new System.Drawing.Point(22, 56);
            this.VerificationBtn.Name = "VerificationBtn";
            this.VerificationBtn.Size = new System.Drawing.Size(116, 42);
            this.VerificationBtn.TabIndex = 2;
            this.VerificationBtn.Text = "verification";
            this.VerificationBtn.UseVisualStyleBackColor = true;
            this.VerificationBtn.Click += new System.EventHandler(this.VerificationBtn_Click_1);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(6, 31);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(207, 160);
            this.listBox1.TabIndex = 4;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.CalculateSumToPaybtn);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.listBox1);
            this.groupBox2.Location = new System.Drawing.Point(419, 24);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(232, 281);
            this.groupBox2.TabIndex = 5;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Medicines";
            // 
            // CalculateSumToPaybtn
            // 
            this.CalculateSumToPaybtn.Location = new System.Drawing.Point(17, 204);
            this.CalculateSumToPaybtn.Name = "CalculateSumToPaybtn";
            this.CalculateSumToPaybtn.Size = new System.Drawing.Size(75, 23);
            this.CalculateSumToPaybtn.TabIndex = 6;
            this.CalculateSumToPaybtn.Text = "ToPay";
            this.CalculateSumToPaybtn.UseVisualStyleBackColor = true;
            this.CalculateSumToPaybtn.Click += new System.EventHandler(this.CalculateSumToPaybtn_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(110, 209);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "PRICE TO PAY";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.RecipeIdTxt);
            this.groupBox1.Controls.Add(this.showMedicinesBnt);
            this.groupBox1.Controls.Add(this.MedicinesOnRecipe);
            this.groupBox1.Controls.Add(this.checkBoxSeal);
            this.groupBox1.Location = new System.Drawing.Point(177, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(222, 293);
            this.groupBox1.TabIndex = 10;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Recipe";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(54, 12);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Recipe Id";
            // 
            // RecipeIdTxt
            // 
            this.RecipeIdTxt.Location = new System.Drawing.Point(57, 28);
            this.RecipeIdTxt.Name = "RecipeIdTxt";
            this.RecipeIdTxt.Size = new System.Drawing.Size(116, 20);
            this.RecipeIdTxt.TabIndex = 6;
            // 
            // showMedicinesBnt
            // 
            this.showMedicinesBnt.Location = new System.Drawing.Point(6, 251);
            this.showMedicinesBnt.Name = "showMedicinesBnt";
            this.showMedicinesBnt.Size = new System.Drawing.Size(75, 23);
            this.showMedicinesBnt.TabIndex = 5;
            this.showMedicinesBnt.Text = "Add Recipe";
            this.showMedicinesBnt.UseVisualStyleBackColor = true;
            this.showMedicinesBnt.Click += new System.EventHandler(this.showMedicinesBnt_Click);
            // 
            // MedicinesOnRecipe
            // 
            this.MedicinesOnRecipe.Location = new System.Drawing.Point(6, 69);
            this.MedicinesOnRecipe.Multiline = true;
            this.MedicinesOnRecipe.Name = "MedicinesOnRecipe";
            this.MedicinesOnRecipe.Size = new System.Drawing.Size(167, 167);
            this.MedicinesOnRecipe.TabIndex = 4;
            // 
            // checkBoxSeal
            // 
            this.checkBoxSeal.AutoSize = true;
            this.checkBoxSeal.Location = new System.Drawing.Point(6, 31);
            this.checkBoxSeal.Name = "checkBoxSeal";
            this.checkBoxSeal.Size = new System.Drawing.Size(47, 17);
            this.checkBoxSeal.TabIndex = 4;
            this.checkBoxSeal.Text = "Seal";
            this.checkBoxSeal.UseVisualStyleBackColor = true;
            // 
            // ShopForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(667, 322);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.VerificationBtn);
            this.Controls.Add(this.EmployeeNameTxt);
            this.Controls.Add(this.label1);
            this.Name = "ShopForm";
            this.Text = "Shop";
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox EmployeeNameTxt;
        private System.Windows.Forms.Button VerificationBtn;
        private System.Windows.Forms.ListBox listBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button CalculateSumToPaybtn;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox RecipeIdTxt;
        private System.Windows.Forms.Button showMedicinesBnt;
        private System.Windows.Forms.TextBox MedicinesOnRecipe;
        private System.Windows.Forms.CheckBox checkBoxSeal;
    }
}