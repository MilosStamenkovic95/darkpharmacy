﻿using ServiceStack.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DarkPharmacy.Entiteti;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System.Windows.Forms;

namespace DarkPharmacy
{
    class RedisMedicine : MedicineBlock
    {
        RedisClient cache;
        public string connectionString;
        public MongoServer server;
        public MongoDatabase db;

        public void prepareConnection()
        {
            connectionString = "mongodb://localhost/?safe=true";
            server = MongoServer.Create(connectionString);
            db = server.GetDatabase("apoteka");
        }

        public RedisMedicine()
        {
            cache = new RedisClient(RedisConfig.SingleHost);
        }

        public MedicineBlock GetMedBlockById(ObjectId id)
        {
            prepareConnection();
            cache.As<MedicineBlock>();
            MedicineBlock meds = cache.Get<MedicineBlock>("MedicineBlock" + id.ToString()); //za trazenje bloka
            if (meds != null)
            {
                cache.Set<MedicineBlock>("MedicineBlock" + meds.id, meds, new TimeSpan(0, 2, 0, 0)); //updatuje ttl
                return meds; //vrati je ili vrati null
            }
            else //kad nije u redis
            {
                MongoCollection medsCollection = db.GetCollection<Medicine>("blok");
                meds = medsCollection.FindOneByIdAs<MedicineBlock>(id);
                cache.Set<MedicineBlock>("MedicineBlock" + meds.id, meds, new TimeSpan(0, 2, 0, 0)); //stavi u redis
                return meds; //vrati je ili vrati null
            }
        }

        public void SetMedBlockCountById(ObjectId id, int newCount)
        {
            prepareConnection();
            cache.As<MedicineBlock>();
            MongoCollection medsCollection = db.GetCollection<Medicine>("blok");
            MedicineBlock meds = medsCollection.FindOneByIdAs<MedicineBlock>(id);
            meds.count = newCount;
            cache.Set<MedicineBlock>("MedicineBlock" + meds.id, meds, new TimeSpan(0, 2, 0, 0));
        }

        public Medicine GetMedById(ObjectId id)
        {
            prepareConnection();
            cache.As<Medicine>();
            Medicine meds = cache.Get<Medicine>("Medicine" + id.ToString()); //za trazenje leka
            if (meds != null)
            {
                cache.Set<Medicine>("Medicine" + meds.name, meds, new TimeSpan(0, 2, 0, 0));
                return meds;
            }
            else
            {
                MongoCollection medsCollection = db.GetCollection<Medicine>("lek");
                meds = medsCollection.FindOneByIdAs<Medicine>(id);
                cache.Set<Medicine>("Medicine" + meds.name, meds, new TimeSpan(0, 2, 0, 0));
                return meds;
            }
        }

        public MedicineStorage GetMedStorageById(ObjectId id)
        {
            prepareConnection();
            cache.As<MedicineStorage>();
            MedicineStorage meds = cache.Get<MedicineStorage>("MedicineStorage" + id.ToString()); //za trazenje skladista
            if (meds != null)
            {
                cache.Set<MedicineStorage>("MedicineStorage" + meds.storageName, meds, new TimeSpan(0, 2, 0, 0));
                return meds;
            }
            else
            {
                MongoCollection medsCollection = db.GetCollection<MedicineStorage>("skladiste");
                meds = medsCollection.FindOneByIdAs<MedicineStorage>(id);
                cache.Set<MedicineStorage>("MedicineStorage" + meds.storageName, meds, new TimeSpan(0, 2, 0, 0));
                return meds;
            }
        }

        public void MedicineExpirationCheck(ObjectId id1, ObjectId id2)
        {
            MedicineBlock mb = GetMedBlockById(id2);
            Medicine med = GetMedById(id1);
            if ((((mb.expirationDate.Year * 12) + mb.expirationDate.Month) - ((System.DateTime.Now.Year * 12) + System.DateTime.Now.Month)) <= 2)
            {
                MessageBox.Show("Lek: " + med.name + " ima jos manje od 2 meseca do isteka roka vazenja.");
            }
        }

        public int CheckReducingAmmounts(ObjectId id1, int blockCount)
        {
            MedicineBlock mb = GetMedBlockById(id1);
            blockCount += mb.count;
            return blockCount;
        }
    }
}
