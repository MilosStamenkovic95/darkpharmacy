﻿namespace DarkPharmacy
{
    partial class StorageForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.CreateStorageBtn = new System.Windows.Forms.Button();
            this.NameOfStorageTxt = new System.Windows.Forms.TextBox();
            this.NameOfStoragelbl = new System.Windows.Forms.Label();
            this.expirationDateMedicine = new System.Windows.Forms.DateTimePicker();
            this.CountMedicines = new System.Windows.Forms.NumericUpDown();
            this.StateCheckBox = new System.Windows.Forms.CheckBox();
            ((System.ComponentModel.ISupportInitialize)(this.CountMedicines)).BeginInit();
            this.SuspendLayout();
            // 
            // CreateStorageBtn
            // 
            this.CreateStorageBtn.Location = new System.Drawing.Point(225, 113);
            this.CreateStorageBtn.Name = "CreateStorageBtn";
            this.CreateStorageBtn.Size = new System.Drawing.Size(176, 23);
            this.CreateStorageBtn.TabIndex = 0;
            this.CreateStorageBtn.Text = "Create Medicine blok in storage";
            this.CreateStorageBtn.UseVisualStyleBackColor = true;
            this.CreateStorageBtn.Click += new System.EventHandler(this.CreateStorageBtn_Click);
            // 
            // NameOfStorageTxt
            // 
            this.NameOfStorageTxt.Location = new System.Drawing.Point(25, 53);
            this.NameOfStorageTxt.Name = "NameOfStorageTxt";
            this.NameOfStorageTxt.Size = new System.Drawing.Size(130, 20);
            this.NameOfStorageTxt.TabIndex = 1;
            // 
            // NameOfStoragelbl
            // 
            this.NameOfStoragelbl.AutoSize = true;
            this.NameOfStoragelbl.Location = new System.Drawing.Point(22, 27);
            this.NameOfStoragelbl.Name = "NameOfStoragelbl";
            this.NameOfStoragelbl.Size = new System.Drawing.Size(133, 13);
            this.NameOfStoragelbl.TabIndex = 2;
            this.NameOfStoragelbl.Text = "Name of Medicine Storage";
            // 
            // expirationDateMedicine
            // 
            this.expirationDateMedicine.Location = new System.Drawing.Point(225, 27);
            this.expirationDateMedicine.Name = "expirationDateMedicine";
            this.expirationDateMedicine.Size = new System.Drawing.Size(200, 20);
            this.expirationDateMedicine.TabIndex = 3;
            // 
            // CountMedicines
            // 
            this.CountMedicines.Location = new System.Drawing.Point(225, 54);
            this.CountMedicines.Name = "CountMedicines";
            this.CountMedicines.Size = new System.Drawing.Size(120, 20);
            this.CountMedicines.TabIndex = 4;
            // 
            // StateCheckBox
            // 
            this.StateCheckBox.AutoSize = true;
            this.StateCheckBox.Location = new System.Drawing.Point(225, 90);
            this.StateCheckBox.Name = "StateCheckBox";
            this.StateCheckBox.Size = new System.Drawing.Size(112, 17);
            this.StateCheckBox.TabIndex = 6;
            this.StateCheckBox.Text = "Is recipe certified?";
            this.StateCheckBox.UseVisualStyleBackColor = true;
            // 
            // StorageForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(438, 153);
            this.Controls.Add(this.StateCheckBox);
            this.Controls.Add(this.CountMedicines);
            this.Controls.Add(this.expirationDateMedicine);
            this.Controls.Add(this.NameOfStoragelbl);
            this.Controls.Add(this.NameOfStorageTxt);
            this.Controls.Add(this.CreateStorageBtn);
            this.Name = "StorageForm";
            this.Text = "Storage";
            ((System.ComponentModel.ISupportInitialize)(this.CountMedicines)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button CreateStorageBtn;
        private System.Windows.Forms.TextBox NameOfStorageTxt;
        private System.Windows.Forms.Label NameOfStoragelbl;
        private System.Windows.Forms.DateTimePicker expirationDateMedicine;
        private System.Windows.Forms.NumericUpDown CountMedicines;
        private System.Windows.Forms.CheckBox StateCheckBox;
    }
}