﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using DarkPharmacy.Entiteti;

namespace DarkPharmacy
{
    public partial class ManagementForm : Form
    {
        public ManagementForm()
        {
            InitializeComponent();
        }

        public string connectionString;
        public MongoServer server;
        public MongoDatabase db;

        public void prepareConnection()
        {
            connectionString = "mongodb://localhost/?safe=true";
            server = MongoServer.Create(connectionString);
            db = server.GetDatabase("apoteka");
        }

        private void EmployeeCollectionBnt_Click_1(object sender, EventArgs e)
        {
            prepareConnection();

            var collection = db.GetCollection<Employee>("radnici");

            MongoCursor<Employee> radnici = collection.FindAll();

            foreach (Employee r in radnici.ToArray<Employee>())
            {
                MessageBox.Show(r.name);
            }
        }

        private void AddEmployeeBtn1_Click(object sender, EventArgs e)
        {
            prepareConnection();

            var collection = db.GetCollection<Employee>("radnici");
           
            Employee radnik1 = new Employee { name = NameTxt.Text, birthDate = EmployeeBirthDate.Value.Date,payment = float.Parse(PaymentTxt.Text) };
            if (intern.Checked==true)
            {
                radnik1.intern = intern.Checked;
            }
            collection.Insert(radnik1);
            MessageBox.Show("Uspesno dodat radnik: " + NameTxt.Text);
        }

        private void DeleteEmpoyeeBtn1_Click(object sender, EventArgs e)
        {
            prepareConnection();

            var collection = db.GetCollection<Employee>("radnici");

            var query = Query.EQ("name", comboBoxEmployee.SelectedItem.ToString());

            collection.Remove(query);

            MessageBox.Show("Izbisan je radnik: " + comboBoxEmployee.SelectedItem.ToString());
        }

        private void comboBoxEmployee_Enter(object sender, EventArgs e)
        {
            comboBoxEmployee.Items.Clear();
            prepareConnection();

            var collection = db.GetCollection<Employee>("radnici");

            MongoCursor<Employee> radnici = collection.FindAll();

            foreach (Employee r in radnici.ToArray<Employee>())
            {
                comboBoxEmployee.Items.Add(r.name);
            }
        }

   
    }
}