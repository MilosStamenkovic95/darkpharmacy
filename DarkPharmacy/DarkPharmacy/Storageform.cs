﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using DarkPharmacy.Entiteti;

namespace DarkPharmacy
{
    public partial class StorageForm : Form
    {
        public StorageForm()
        {
            InitializeComponent();
        }

        public string connectionString;
        public MongoServer server;
        public MongoDatabase db;

        public void prepareConnection()
        {
            connectionString = "mongodb://localhost/?safe=true";
            server = MongoServer.Create(connectionString);
            db = server.GetDatabase("apoteka");
        }

        private void CreateStorageBtn_Click(object sender, EventArgs e)
        {
            prepareConnection();

            //get storage
            MedicineStorage medicineStorage = db.GetCollection<MedicineStorage>("skladiste").AsQueryable<MedicineStorage>().Where(s => s.storageName == NameOfStorageTxt.Text).FirstOrDefault();
            //parse block
            MedicineBlock addBlock = new MedicineBlock
            {
                count = Convert.ToInt32(CountMedicines.Value),
                expirationDate = expirationDateMedicine.Value,
                state = StateCheckBox.Checked,
                treshholdState = expirationDateMedicine.Value.Month - System.DateTime.Now.Month
                //treshholdState=11
            };
            //must exist in collection before being added in list by ref
            db.GetCollection<MedicineStorage>("blok").Insert(addBlock);
            //add to list
            MongoDBRef referenceToBlock = new MongoDBRef("skladiste", addBlock.id);
            medicineStorage.medicineBlocks.Add(referenceToBlock);
            //update storage
            db.GetCollection<MedicineStorage>("skladiste").Save(medicineStorage);
        }
    }
}
