﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

using MongoDB.Driver;
using MongoDB.Bson;
using MongoDB.Driver.Builders;
using MongoDB.Driver.Linq;
using DarkPharmacy.Entiteti;

namespace DarkPharmacy
{
    public partial class MedicinesForm : Form
    {
        public MedicinesForm()
        {
            InitializeComponent();
        }

        public string connectionString;
        public MongoServer server;
        public MongoDatabase db;

        public void prepareConnection()
        {
            connectionString = "mongodb://localhost/?safe=true";
            server = MongoServer.Create(connectionString);
            db = server.GetDatabase("apoteka");
        }

        private void DeleteMedicines_Click(object sender, EventArgs e)
        {
            prepareConnection();
            var collection = db.GetCollection<Medicine>("lek");
            var query = Query.EQ("name", comboBoxMedic.SelectedItem.ToString());
            collection.Remove(query);
            MessageBox.Show("Izbisan je lek: " + comboBoxMedic.SelectedItem.ToString());
        }

        private void AddMedicinesBtn_Click(object sender, EventArgs e)
        {
            prepareConnection();
           
            var collection = db.GetCollection<Medicine>("lek");
            var collectionStorage = db.GetCollection<MedicineStorage>("skladiste");
          
              
            Medicine medic = new Medicine
             {
                name = NameTxt.Text,
                scientificName = scientificNameTxt.Text,
                price = float.Parse(priceTxt.Text),
                recipePrice = float.Parse(recipePricetxt.Text),

            };
            if (IsThereUnwantedEffectChckBox.Checked == true)
            {
                medic.unwantedEffect = new List<string>();
                if (unwantedEffectTxt.Text != "")
                    medic.unwantedEffect.Add(unwantedEffectTxt.Text);

                foreach (string item in ChckListUnwantedEffect.CheckedItems)
                {
                    medic.unwantedEffect.Add(item);
                }
            }
           

            MedicineStorage medicStorage = new MedicineStorage();
            medicStorage.storageName = NameTxt.Text;

            int medicineCount = 0;
            int medicineStorageCount = 0;

            MongoCursor<Medicine> allMedicines = collection.FindAll();
            foreach (Medicine m in allMedicines.ToArray<Medicine>())
            {
                if (m.name==medic.name)
                {
                    medicineCount++;
                }
            }

            MongoCursor<MedicineStorage> allMedicineStorages = collectionStorage.FindAll();
            foreach (MedicineStorage mst in allMedicineStorages.ToArray<MedicineStorage>())
            {
                if (mst.storageName == medic.name)
                {
                    medicineStorageCount++;
                }
            }

            if (medicineCount==0)
            {
                collection.Insert(medic);
                MessageBox.Show("Uspesno dodat lek: " + NameTxt.Text);
            }
            else
            {
                MessageBox.Show("Vec postoji lek sa nazivom: " + NameTxt.Text);
            }

            if (medicineStorageCount == 0)
            {
                collectionStorage.Insert(medicStorage);
                MessageBox.Show("Uspesno dodato skladiste: " + NameTxt.Text);
            }
            else
            {
                MessageBox.Show("Vec postoji skladiste sa nazivom: " + NameTxt.Text);
            }
        }

        private void MedicinesCollectionBnt_Click(object sender, EventArgs e)
        {
            prepareConnection();
            var collection = db.GetCollection<Medicine>("lek");

            MongoCursor<Medicine> medicines = collection.FindAll();

            foreach (Medicine m in medicines.ToArray<Medicine>())
            {
                MessageBox.Show(m.name);
            }
        }

        private void comboBoxMedic_Enter(object sender, EventArgs e)
        {
            comboBoxMedic.Items.Clear();
            prepareConnection();

            var collection = db.GetCollection<Medicine>("lek");
            MongoCursor<Medicine> medicines = collection.FindAll();
            foreach (Medicine m in medicines.ToArray<Medicine>())
            {
                comboBoxMedic.Items.Add(m.name);
            }
        }

        private void searchMedicineByNamebtn_Click(object sender, EventArgs e) //vraca cenu leka za uneto ime leka
        {
            prepareConnection();

            var lek = db.GetCollection<Medicine>("lek").AsQueryable<Medicine>().Where(m => m.name == NameSearchMedictxt.Text).FirstOrDefault();
            if (lek == null)
            {
                MessageBox.Show("No results");
            }
            else
            {
                MessageBox.Show(lek.price.ToString());
            }
        }

        private void UpdateMedicineBtn_Click(object sender, EventArgs e)
        {
            prepareConnection();

            var collection = db.GetCollection<Medicine>("lek");
            var query = Query.EQ("name", NameSearchMedictxt.Text);
            var update = MongoDB.Driver.Builders.Update.Set("price", BsonValue.Create(float.Parse(NewPriceTxt.Text)));
            collection.Update(query, update);
            foreach (Medicine m in collection.Find(Query.EQ("name", BsonValue.Create(NameSearchMedictxt.Text))))
            {
                 MessageBox.Show(m.name + ":" + m.price);
                
            }
        }

        private void CountMedicinesBnt_Click_1(object sender, EventArgs e)
        {
            prepareConnection();
            var collection = db.GetCollection<Medicine>("lek");
            var result1 = (from lek in collection.AsQueryable<Medicine>()
                           select lek).Count();
            MessageBox.Show(result1.ToString());
        }

        private void SortByNameBnt_Click(object sender, EventArgs e)
        {
            prepareConnection();

            var collection = db.GetCollection<Medicine>("lek");
            string rezultat = "";
            foreach (Medicine r in collection.FindAll().SetSortOrder(new string[] { "name" })) //ILI: //collection.FindAll().SetSortOrder(SortBy.Descending("prezime"));
            {
                
                rezultat = rezultat + r.name + "\n";
            }
            MessageBox.Show(rezultat);
        }

        private void SortByPriceBnt_Click(object sender, EventArgs e)
        {
            prepareConnection();

            var db = server.GetDatabase("apoteka");
            var collection = db.GetCollection<Medicine>("lek");
            string rezultat = "";
            foreach (Medicine r in collection.FindAll().SetSortOrder(new string[] { "price" })) //ILI: //collection.FindAll().SetSortOrder(SortBy.Descending("prezime"));
            {
                
                rezultat = rezultat + r.name+ ":" + r.price + "\n";
            }
            MessageBox.Show(rezultat);
        }

        private void MatchByNameAndPrice_Click(object sender, EventArgs e)
        {
            prepareConnection();

            var collection = db.GetCollection<Medicine>("lek");
            //var query = Query.And(
            //                Query.Not(Query.EQ("name", NameSearchMedictxt.Text)),
            //                Query.GT("price", NewPriceTxt.Text)
            //                );

            var Rezultat = db.GetCollection<Medicine>("lek").AsQueryable<Medicine>().Where(m => m.name != NameSearchMedictxt.Text && m.price>= Convert.ToUInt32(NewPriceTxt.Text));

            if (Rezultat == null)
            {
                MessageBox.Show("No results");
            }
            else
            {
                string rezultatUpita = "";

                foreach (Medicine m in Rezultat)
                    rezultatUpita = rezultatUpita + m.name + "\n";

                MessageBox.Show(rezultatUpita);
            }
            //foreach (Medicine r in collection.Find(query))
            //{
            //    MessageBox.Show(r.name + " " + r.price);
            //}
            //MessageBox.Show("Neuspelo");
        }

        private void ReturnFirstBnt_Click(object sender, EventArgs e)
        {
            prepareConnection();

            var collection = db.GetCollection<Medicine>("lek");
            string rezultat = "";
            foreach (Medicine r in collection.FindAll().SetLimit(2))
            {
                rezultat = rezultat + r.name + "\n";

               
            }
            MessageBox.Show(rezultat);
        }

        private void ReturnOtherBnt_Click(object sender, EventArgs e)
        {
            prepareConnection();

            var collection = db.GetCollection<Medicine>("lek");
            string rezultat = "";
            foreach (Medicine r in collection.FindAll().SetSkip(2).SetLimit(3))
            {
                rezultat = rezultat + r.name + "\n";


            }
            MessageBox.Show(rezultat);
        }

        private void ExpCheckBtn_Click(object sender, EventArgs e)
        {
            prepareConnection();

            var MedCollection = db.GetCollection<Medicine>("lek");
            MongoCursor<Medicine> medicines = MedCollection.FindAll();
            var MedBlockCollection = db.GetCollection<MedicineBlock>("blok");
            MongoCursor<MedicineBlock> medicineBlocks = MedBlockCollection.FindAll();
            var MedStorageCollection = db.GetCollection<MedicineStorage>("skladiste");
            MongoCursor<MedicineStorage> medicineStorages = MedStorageCollection.FindAll();

            foreach (Medicine mediID in medicines.ToArray<Medicine>())
            {
                foreach (MedicineStorage storID in medicineStorages.ToArray<MedicineStorage>())
                {
                    if (mediID.name==storID.storageName)
                    {
                        foreach (MongoDBRef blocID in storID.medicineBlocks)
                        {
                            RedisMedicine redMed = new RedisMedicine();
                            redMed.MedicineExpirationCheck(mediID.Id, blocID.Id.AsObjectId);
                        }
                    }
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }
    }
}


