﻿namespace DarkPharmacy
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.ManagementBtn = new System.Windows.Forms.Button();
            this.ShopBnt = new System.Windows.Forms.Button();
            this.StorageBtn = new System.Windows.Forms.Button();
            this.MedicinesBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(189, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(395, 73);
            this.label1.TabIndex = 0;
            this.label1.Text = "PHARMACY";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // ManagementBtn
            // 
            this.ManagementBtn.Location = new System.Drawing.Point(535, 275);
            this.ManagementBtn.Name = "ManagementBtn";
            this.ManagementBtn.Size = new System.Drawing.Size(161, 34);
            this.ManagementBtn.TabIndex = 1;
            this.ManagementBtn.Text = "Management";
            this.ManagementBtn.UseVisualStyleBackColor = true;
            this.ManagementBtn.Click += new System.EventHandler(this.ManagementBtn_Click);
            // 
            // ShopBnt
            // 
            this.ShopBnt.Location = new System.Drawing.Point(535, 152);
            this.ShopBnt.Name = "ShopBnt";
            this.ShopBnt.Size = new System.Drawing.Size(161, 34);
            this.ShopBnt.TabIndex = 2;
            this.ShopBnt.Text = "Shop";
            this.ShopBnt.UseVisualStyleBackColor = true;
            this.ShopBnt.Click += new System.EventHandler(this.ShopBnt_Click);
            // 
            // StorageBtn
            // 
            this.StorageBtn.Location = new System.Drawing.Point(79, 275);
            this.StorageBtn.Name = "StorageBtn";
            this.StorageBtn.Size = new System.Drawing.Size(161, 34);
            this.StorageBtn.TabIndex = 3;
            this.StorageBtn.Text = "Storage";
            this.StorageBtn.UseVisualStyleBackColor = true;
            this.StorageBtn.Click += new System.EventHandler(this.StorageBtn_Click);
            // 
            // MedicinesBtn
            // 
            this.MedicinesBtn.Location = new System.Drawing.Point(79, 152);
            this.MedicinesBtn.Name = "MedicinesBtn";
            this.MedicinesBtn.Size = new System.Drawing.Size(161, 34);
            this.MedicinesBtn.TabIndex = 4;
            this.MedicinesBtn.Text = "Medicines";
            this.MedicinesBtn.UseVisualStyleBackColor = true;
            this.MedicinesBtn.Click += new System.EventHandler(this.MedicinesBtn_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Teal;
            this.ClientSize = new System.Drawing.Size(784, 365);
            this.Controls.Add(this.MedicinesBtn);
            this.Controls.Add(this.StorageBtn);
            this.Controls.Add(this.ShopBnt);
            this.Controls.Add(this.ManagementBtn);
            this.Controls.Add(this.label1);
            this.Name = "Form1";
            this.Text = "Pharma";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button ManagementBtn;
        private System.Windows.Forms.Button ShopBnt;
        private System.Windows.Forms.Button StorageBtn;
        private System.Windows.Forms.Button MedicinesBtn;
    }
}

